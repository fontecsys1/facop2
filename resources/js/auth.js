/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./filter');
window.Vue = require('vue');
import VueEvents from 'vue-events'
Vue.use(VueEvents)

Vue.component('flash', require('./components/Flash.vue').default);
window.Form =  require('./classes/Form').default

Vue.component('login', require('./auth/Login.vue').default);
Vue.component('register', require('./auth/Register.vue').default);

Vue.component('reset-password', require('./auth/ResetPassword.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const auth = new Vue({
  el: '#auth',
  mounted()
  {

  },
  created()
  {
  },
  data()
  {
    return{
    }
  },
  methods:{
          ui()
          {
            $.blockUI({   message:'<div class="kt-spinner kt-spinner--v2 kt-spinner--danger big "></div>',
            css: {
                    backgroundColor: '#f00',
                    color: '#fff',
                    position: 'fixed',
                    padding: '0',
                    margin: '0px',
                    width: 'auto',
                    top: '50%',
                    left: '50%',
                    color:'rgb(0, 0, 0)',
                        border: 'none',
                    cursor: 'wait',
                  },
            overlayCSS:
              {
              backgroundColor: 'rgba(255, 255, 255,1)',
              opacity:         1,
              cursor:          'wait'
              },
         });
          }
  },
  watch:
   {


   }
});
