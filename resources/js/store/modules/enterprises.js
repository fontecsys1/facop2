const state  =
{
    paginator:{},
    items:[],
    enterprise:null,
    loading:false,
    loaded:false,
    currentPage:0,
    enterpriseAsParameter:[],
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      enterpriseAsParameter: state => {

        return state.enterpriseAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    enterprise: state => {
      return state.enterprise
  },
};

const actions =
{
  async getEnterprise({commit},payload)
  {
     commit('SET_LOADING',true)

     var result = await axios.get('/api/entreprises/'+payload).then( response =>
          {
            commit('SET_ENTERPRISE',response.data)
            commit('SET_LOADING',false)
          })
      return result;
  },
   async getEnterprises({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

//fr
       var result = await axios.get(payload.page=="" ? '/api/entreprises/' :'/api/entreprises'+payload.query).then( response =>
            {
              commit('SET_ENTERPRISES',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateEnterprise({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/entreprises/'+payload.id,payload).then( response =>
            {
              commit('UPDATE_ENTERPRISE',response.data.entreprise)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addEnterprise({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/entreprises',payload).then( response =>
            {
              commit('ADD_ENTERPRISE',response.data.entreprise)
              commit('SET_LOADING',false)

            })

        return await result

    },
    async enterpriseAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)
       var result = await axios.get('/entreprises/enterpriseAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              // alert(data)


              commit('ENTERPRISE_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeEnterprise({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/entreprises/'+payload.id).then( response =>
            {
              commit('REMOVE_ENTERPRISE',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_ENTERPRISES',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_ENTERPRISES(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true

        }


    },
    UPDATE_ENTERPRISE(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_ENTERPRISE(state,payload)
    {
            state.items.unshift(payload)
    },
    REMOVE_ENTERPRISE(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },
    ENTERPRISE_AS_PARAMETER(state,payload)
    {
          // alert("payload"+payload)
          state.enterpriseAsParameter = payload ? payload : new Array()
    },

    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_ENTERPRISE(state,payload)
    {
        state.enterprise = payload
    },
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
