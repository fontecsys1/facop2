
const state  =
{
    paginator:{},
    items:[],
    member:null,
    loading:false,
    loaded:false,
    memberAsParameter:[],
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      memberAsParameter: state => {
        return state.memberAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    member: state => {
      return state.member
  },
};

const actions =
{
   async getMembers({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       await axios.get(payload.page=="" ? '/api/membres/' :'/api/membres'+payload.query).then( response =>
            {
              console.log("Promise Rejected:")

              commit('SET_MEMBERS',response.data)
              commit('SET_LOADING',false)
            })
    },
    async updateMember({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/membres/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_MEMBER',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addMember({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.post('/api/membres',payload).then( response =>
            {
              commit('ADD_MEMBER',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async getMember({commit},payload)
    {
       commit('SET_LOADING',true)
//
       var result = await axios.get('/api/membres/'+payload).then( response =>
            {
              commit('SET_MEMBER',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async memberAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)

       var result = await axios.get('/membres/memberAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              console.log(data)
              commit('MEMBER_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeMember({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/membres/'+payload.id).then( response =>
            {
              commit('REMOVE_MEMBER',response.data.membre)
              commit('SET_LOADING',false)
            })
        return result;
    },

    init({commit})
    {
            commit('SET_MEMBER',{})
            commit('SET_MEMBERS',{})
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_MEMBERS(state,payload)
    {
      console.log("loaded ok")

        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []
            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
             state.items.push(element)
           });
           state.loaded = true
           console.log("loaded ok")

        }


    },
    UPDATE_MEMBER(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_MEMBER(state,payload)
    {
          state.items.unshift(payload)
    },
    MEMBER_AS_PARAMETER(state,payload)
    {
          console.log("paykoad"+payload)
          state.memberAsParameter = payload ? payload : new Array()
    },
    REMOVE_MEMBER(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },

    SET_MEMBER(state,payload)
    {
        state.member = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    }
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
