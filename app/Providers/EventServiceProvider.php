<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\DocsOnVehicleCreated' => [
            'App\Listeners\UpdateFileProperty',
        ],
        'App\Events\CashAdvancedHasBeenMadeEvent' => [
            'App\Listeners\AddFluxFinanceItem',
        ],
        'App\Events\GalleryOnVehicleCreated' => [
            'App\Listeners\GalleryProperty',
        ],
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\UpdateLastSignIn',
        ],
        'Illuminate\Auth\Events\Logout' => [
            'App\Listeners\UpdateLastSignOut',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
