<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded=[];

        public function user()
       {
           return $this->belongsTo('App\User','membre_id');
       }

       public function projet()
       {
           return $this->belongsTo('App\projet','projet_id');
       }
}
