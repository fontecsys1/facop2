<?php

namespace App;

use App\Traits\CanUpload;
use App\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,HasRoles,CanUpload,LogsActivity,HasApiTokens;

    // protected $guard ="admins";
    protected $guarded=[];


    protected static $logAttributes = ["name","email","description",'role_id'];
    protected static $logName = 'utilisateur';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;


    protected $storage_path ="public/avatars";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['hasAvatar',"permissions","hasPhoto",'fullName','fullNameWithTitel'];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé l'utilisateur <strong>{$this->name}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié  l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié l'utilisateur <strong>{$this->name}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté  l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté  l'utilisateur <strong>{$this->name}</strong>";
        }

    }

    public function getHasAvatarAttribute()
    {
        if($this->avatar==null)
        return false;
        else
        {
            return file_exists(storage_path("/app/public/avatars/".$this->avatar));
        }
    }
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getPermissionsAttribute()
    {
           return $this->permissions();
    }


    public function permissions()
    {
      if(!$this->hasAnyRole())
          return collect([]);
      else
        return $this->role->hasPermissions() ? $this->role->permissions->flatten()->pluck("nom") : collect([]);
    }

    public function hasAnyRole()
    {
      return $this->role!=null ? true : false ;
    }

    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('users.email', 'LIKE', "%{$q}%")
                ->orWhere('users.name', 'LIKE', "%{$q}%")
                ->orWhere('users.created_at', 'LIKE', "%{$q}%")
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
                ->orWhere('users.nom', 'LIKE', "%{$q}%")
                ->orWhere('users.derniere_profession', 'LIKE', "%{$q}%")
                ->orWhere('users.activite_actuelle', 'LIKE', "%{$q}%")
                ->orWhere('users.phone1', 'LIKE', "%{$q}%")
                ->orWhere('users.phone2', 'LIKE', "%{$q}%")
                ->orWhere('users.prenom', 'LIKE', "%{$q}%")
                ->orWhere('users.statut_marital', 'LIKE', "%{$q}%");
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }
    public function scopeFilterRole($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('users.role_id', $q);
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }

            /**
     * Get all of the user's enterprises.
     */
    public function entreprises()
    {
        return $this->morphMany('App\Entreprise', 'creatable');
    }


    public function getHasPhotoAttribute()
    {
        if($this->avatar == null) return false;
        return file_exists(storage_path("/app/public/featured/".$this->avatar)) ? true : false;
    }
    public function getFullNameAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }

    public function getFullNameWithTitelAttribute()
    {
        return ucfirst($this->civilite)." ".ucfirst($this->prenom)." ".ucfirst($this->nom);
    }
    public function scopeSecteur($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('users.secteur_activite_id',$q);
    }

    public function scopeSearchAsParameter($query, $q)
    {
        if ($q == null) return $query;
        return $query->orWhere('users.nom', 'LIKE', "%{$q}%")
                     ->orWhere('users.prenom', 'LIKE', "%{$q}%");
    }

    public function scopePromotion($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('users.promotion',$q);
    }

    public function nationalites()
    {
        return $this->belongsToMany("App\Pays", 'nationalite_membre', 'membre_id', 'nationalite_id');

    }

    public function competences()
    {
        return $this->belongsToMany("App\Competence", 'competence_membre', 'membre_id', 'competence_id');

    }

    public function residence()
    {
        return $this->belongsTo("App\Pays","pays_residence_id");

    }

    public function formation()
    {
        return $this->belongsTo("App\Formation");

    }
    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite","secteur_activite_id");

    }


    public function comments()
    {
        return $this->hasMany('App\Comment', 'membre_id');
    }



    /**
     * Get all of the member's enterprises.
     */
    public function projets()
    {
        return $this->morphMany('App\Projet', 'creatable');
    }
}
