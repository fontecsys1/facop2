<?php

namespace App\Exports;

use App\Entreprise;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class EntrepriseExport implements WithEvents, WithTitle, FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize
{          

    use Exportable;
    private $search,$deroulement,$payement;
    
    public function title(): string
    {
        return 'Entreprises';
    }
    public function __construct($search,$deroulement,$payement)
    {
        $this->search =$search;
        $this->deroulement =$deroulement;
        $this->payement =$payement;
    }
    //use Exportable; 
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return Entreprise::query()->search($this->search)->deroulement($this->deroulement)->statut($this->payement);
    }

    public function map($Entreprise): array
    {
        return [
            "Entreprise du ".Carbon::parse($Entreprise->date_dbt)->format('d/m/Y')." au ".Carbon::parse($Entreprise->date_fin)->format('m/d/Y'),
            $Entreprise->nbr_jour,
            Carbon::parse($Entreprise->date_dbt)->format('m/d/Y'),
            Carbon::parse($Entreprise->date_fin)->format('m/d/Y'),
            $Entreprise->statutPayement->nature,
            $Entreprise->en_cours,
            $Entreprise->vehicule->libelle,
            $Entreprise->client->nomComplet,
            $Entreprise->montant,
            

        ];
    }

    public function headings(): array
    {
        return [
            "descriptif",
            "Nombre de jour",
            'Date de début',
            'Date de fin',
            "Statut du payement",
            "Déroulement",
            'voiture',
            'Client',
            "Montant",
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'H' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function registerEvents(): array
    {
        $center = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]];
        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_NONE,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
        return [
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray,$center) {
                $cellRange = 'A1:J1'; // All headers
              $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
              $event->sheet->getDelegate()->getStyle("B1:D1")->applyFromArray($center);

            },
        ];
    }

}
