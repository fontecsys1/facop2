<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Pays extends Model
{
    protected $guarded=[];
    use LogsActivity;

    protected static $logAttributes = ["libelle"];
    protected static $logName = 'pays';
    protected static $logOnlyDirty = true;   protected static $submitEmptyLogs = false;

    protected $appends =["hasFlag","flagPath"];

    public function getHasFlagAttribute()
    {
        if($this->code2 == null) return false;
        return file_exists(storage_path("/app/public/flags/".strtolower($this->code2).'.png')) ? true : false;
    }
    
    public function getFlagPathAttribute()
    {
        return $this->hasFlag ? strtoLower($this->code2).'.png' : "unknown.png";
    }
}
