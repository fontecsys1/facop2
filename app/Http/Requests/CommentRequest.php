<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'projet_id' =>"required",
            'content' => "required",
        ];
    }


    public function messages()
    {
        return [

            'projet_id' =>"Le projet est requis",
            'content' =>"Le un commentaire est est requise",
         ];
    }
}
