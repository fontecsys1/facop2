<?php

namespace App\Http\Controllers;

use App\User;
use App\Projet;
use App\Entreprise;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projet = $this->countProjet();
        return [
            "nombre_projet"=>$projet,
            "projet_en_cours"=>$this->countProjetEncours('ouvert'),
            "projet_termine"=>$this->countProjetEncours('fermé'),
            "nombre_entreprise"=>$this->countEntreprise(),
            'secteur_plus_represente'=>$this->SecteurLePlusRepresente(),
            "effectif_moyen" => 5,
            "effectif_total" => 6,
            "nombre_membre"=>$this->countMembre(),
        ];
    }

    private function countProjet(){
        return Projet::all()->count();
    }

    private function countProjetEncours($status){
        return Projet::whereStatut($status)->count();
    }

    private function countEntreprise(){
        return Entreprise::all()->count();
    }

    private function SecteurLePlusRepresente(){
        return Entreprise::all()->groupBy('effectif')->max();
    }

    private function countMembre(){
        return User::all()->count();
    }

}
