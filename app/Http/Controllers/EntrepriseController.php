<?php

namespace App\Http\Controllers;

use App\Adresse;
use App\Contact;
use App\Exports\EntrepriseExport;
use App\Entreprise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EntrepriseRequest;
use App\ViewRecords\VehicleRecord;
use App\VisiteTechnique;
use Carbon\Carbon;
use DB;
use Log;
use Excel;
use App\Traits\CanUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Log as FacadesLog;

class EntrepriseController extends Controller
{

    use CanUpload;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 8 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $fj = request()->query('filter_forme_juridique') == null ? null : request()->query('filter_forme_juridique');
        $ps = request()->query('filter_pays') == null ? null : request()->query('filter_pays');
        $secteur = request()->query('filter_secteur') == null ? null : request()->query('filter_secteur');

        return   Entreprise::with(['pays','secteur','responsable.competences','responsable.secteur','responsable.formation','responsable.residence','responsable.nationalites','creatable'])->search($q)->secteur($secteur)->pays($ps)->formeJuridique($fj)->orderBy("created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\EntrepriseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntrepriseRequest $request)
    {
        if($request->input('step')==3 || $request->input('step')=="3")
        {

            //ajout des options
            //ajout des images
            //ajout des document

            try
            {

                //on commence la transaction
                DB::beginTransaction();

                    Log::info("On commence la transaction");
                    //Ajout de l'image
                    $fileName = null;
                    if($request->input('logo')!="" && $request->input('logo')!=null)
                    {
                        $fileName =  $this->uploadAvatar($request->input('logo'),$this->getMime($request->input('logo')));
                    }
                   //ajout de l'Entreprise
                    $entreprise = Entreprise::create(
                        [
                            //Step 0
                            "nom_commercial"=>$request->input('nom_commercial'),
                            'forme_juridique' =>$request->input('forme_juridique'),
                            'secteur_activite_id' => strtolower($request->input('secteur_activite_id')),
                            'secteur_activite_principal' =>$request->input('secteur_activite_principal'),
                            'autre_activite' =>$request->input('autre_activite'),
                            'effectif' =>$request->input('effectif'),
                            'pays_id' =>$request->input('pays_id'),
                            'responsable_id' =>$request->input('responsable_id'),
                            'logo' =>$fileName,
                            'ville'=>$request->input('ville'),
                            'quartier'=>$request->input('quartier'),
                            'nr'=>$request->input('nr'),
                            'rue'=>$request->input('rue'),
                            'bp'=>$request->input('bp'),
                            'phone1' =>$request->input('phone1'),
                            'phone2'=>$request->input('phone2'),
                            'siteweb'=>$request->input('siteweb'),
                            'email'=>$request->input('email'),
                            'facebook'=>$request->input('facebook'),
                            'twitter'=>$request->input('twitter'),
                            'linkedin'=>$request->input('linkedin'),
                            'creatable_type'=>"App\User",
                            'creatable_id'=>Auth::user()->id,
                        ]
                    );





                DB::commit();

                return ['success'=>true,'entreprise'=>$entreprise->load(['secteur','pays','responsable'])];
            }
            catch(\Exception $e)
            {
                    DB::rollback();
                    return ['status'=>false,'message'=>$e->getMessage()];

            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return \Illuminate\Http\Response
     */
    public function show(Entreprise $entreprise)
    {
         return $entreprise->load(['secteur','pays','responsable']);
    }

    public function getAsParams()
    {
        $q = request()->query('query') == null ? null : request()->query('query');
        return  $q ?  Entreprise::select('id','entreprises.nom_commercial')->searchAsParameter($q)->paginate(8) : null ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return \Illuminate\Http\Response
     */
    public function edit(Entreprise $Entreprise)
    {
        //
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entreprise  $Entreprise
     * @return \Illuminate\Http\Response
     */
    public function update(EntrepriseRequest $request, Entreprise $entreprise)
    {
        if($request->input('step')==3 || $request->input('step')=="3")
        {


        try
        {
            DB::beginTransaction();

            if($request->input('logoMustBeUpdated'))
            {
                //on retire l'ancienne image s'il y en a
                if($entreprise->hasPhoto)
                {
                    if(file_exists(storage_path('app/public/images/featured/'.$entreprise->logo)))
                    {
                        unlink(storage_path('app/public/images/featured/'.$entreprise->logo));
                    }
                }
                //on charge la photo
                if($request->input('logo'))
                {

                    $fileName =  $this->uploadAvatar($request->input('logo'),$this->getMime($request->input('logo')));
                    $entreprise->logo =$fileName ;
                    Log::info($fileName." OK mon lapin" );
                }
                else
                {
                    //on retire la photo
                    $entreprise->logo = null;
                }

            }

            $entreprise->nom_commercial =$request->input("nom_commercial");
            $entreprise->forme_juridique =$request->input("forme_juridique");
            $entreprise->secteur_activite_id =$request->input("secteur_activite_id");
            $entreprise->secteur_activite_principal =$request->input("secteur_activite_principal");
            $entreprise->autre_activite =$request->input("autre_activite");
            $entreprise->effectif =$request->input("effectif");
            $entreprise->responsable_id =$request->input('responsable_id');


            $entreprise->ville =$request->input("ville");
            $entreprise->rue =$request->input("rue");
            $entreprise->nr =$request->input("nr");
            $entreprise->quartier =$request->input("quartier");
            $entreprise->bp =$request->input("bp");
            $entreprise->pays_id =$request->input("pays_id");

            $entreprise->phone1 =$request->input("phone1");
            $entreprise->phone2=$request->input("phone2");
            $entreprise->siteweb=$request->input("siteweb");
            $entreprise->email=$request->input("email");
            $entreprise->facebook=$request->input("facebook");
            $entreprise->twitter=$request->input("twitter");
            $entreprise->linkedin=$request->input("linkedin");
            $entreprise->save();





            DB::commit();
            return response()->json(['success' => true,'entreprise'=> $entreprise->load(['pays',"secteur",'responsable'])],200);

        }catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
      }
    }

    private function getEntreprise($id)
    {
        return  Entreprise::with(['contact','adresse','secteur'])->whereId($id)->first();


    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entreprise  $Entreprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entreprise $Entreprise)
    {
        //on supprime
        $Entreprise->delete();
        return response()->json(['message' => 'Entreprise supprimé avec succès'],200);
    }

    public function export()
    {

        // $q = request()->query('filter') == null ? null : request()->query('filter');
        // $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        // $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        // $annee= request()->query('annee') == null ? null : request()->query('annee');
        // $mois = request()->query('mois') == null ? null : request()->query('mois');
        // $Entreprise = request()->query('Entreprise_id') == null ? null : request()->query('Entreprise_id');

        $q = request()->query('q') == null ? null : request()->query('q');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "voitures-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new EntrepriseExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new EntrepriseExport($q),$filename.'.pdf');
        }

    }
}
