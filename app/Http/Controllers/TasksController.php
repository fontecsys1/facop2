<?php

namespace App\Http\Controllers;

use App\Task;
use App\Projet;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{

    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page"):10;
        // if(request()->expectsJSON())
        return   Task::with(['projet','creatable'])->orderBy("created_at",'desc')->paginate($per);
    }


    public function create()
    {
        //
    }

    public function store(TaskRequest $request)
    {
        try
        {

            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour cette tâche";
            }


            if($request->input("avancement") == 0){
                $statut = "En cours";
            }else if($request->input("avancement") > 0 && $request->input("avancement") < 100){
                $statut = "En cours";
            }else if($request->input("avancement") == 100){
                $statut = "Achevée";
            }else{
                $statut = "Abandonnée";
            }

            $task = Task::create(
                [
                    'name' =>$request->input('name'),
                    'date_debut_prev' =>$request->input("date_debut_prev"),
                    'date_fin_prev' =>$request->input("date_fin_prev"),
                    'avancement' =>$request->input('avancement'),
                    'statut' =>$statut,
                    'projet_id' =>$request->input("projet_id"),
                    'description' => $description,
                    'creatable_type'=>"App\User",
                    'creatable_id'=>Auth::user()->id,
                ]
                );

            DB::commit();
            // mise à jour de l'avancement du projet
            $avancementProjet = DB::table('tasks')->where('projet_id',$request->input("projet_id"))->avg('avancement');

            $projet = Projet::findOrFail($request->input("projet_id"));
            $projet->avancement = $avancementProjet;
            $projet ->save();

            // dd($projet);

            return response()->json(['success' => true,'task'=> $task, 'projet' => $projet->load(['tasks','entreprise','creatable','comments'])]);

        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }


    public function update(TaskRequest $request, Task $task)
    {
        try
        {
            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour cette tâche";
            }


            if($request->input("avancement") == 0){
                $statut = "En attente";
            }else if($request->input("avancement") > 0 && $request->input("avancement") < 100){
                $statut = "En cours";
            }else if($request->input("avancement") == 100){
                $statut = "Achevée";
            }else{
                $statut = "Abandonnée";
            }

            $task->name = $request->input('name');
            $task->date_debut_prev = $request->input("date_debut_prev");
            $task->date_fin_prev = $request->input("date_fin_prev");
            $task->avancement = $request->input('avancement');
            $task->projet_id = $request->input("projet_id");
            $task->description = $description;
            $task->statut = $statut;

            $task->creatable_type = "App\User";
            $task->creatable_id= Auth::user()->id;
            $task->save();

            DB::commit();
             // mise à jour de l'avancement du projet
             $avancementProjet = DB::table('tasks')->where('projet_id',$request->input("projet_id"))->avg('avancement');

             $projet = Projet::findOrFail($request->input("projet_id"));
             $projet->avancement = $avancementProjet;
             $projet ->save();

             return response()->json(['success' => true,'task'=> $task, 'projet' => $projet->load(['tasks','entreprise','creatable','comments'])]);
            }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }


    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $projet_id = $task->projet_id;
        $task->delete();

         // mise à jour de l'avancement du projet
         $avancementProjet = DB::table('tasks')->where('projet_id',$projet_id)->avg('avancement');
            if(!$avancementProjet){
                $avancementProjet = "0";
            }
         $projet = Projet::findOrFail($projet_id);

         $projet->avancement = $avancementProjet;
         $projet ->save();

         return response()->json(['success' => true,'task'=> $task, 'projet' => $projet->load(['tasks','entreprise','creatable','comments'])]);
        }
}
