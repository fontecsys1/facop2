<?php

namespace App\Http\Controllers\API;

use App\FluxFinance;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GraphController extends Controller
{
    public function chiffre_affaire_graph()
    {

        return [ "labels"=>[] ,"ca"=>  [],"bars"=>[]];

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $d_fin = request()->query('filter_date_fin') == null ? null : request()->query('filter_date_fin');
        $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        $annee= request()->query('annee') == null ? null : request()->query('annee');
        $mois = request()->query('mois') == null ? null : request()->query('mois');
        $since = request()->query('since') == null ? null : request()->query('since');
        $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        //Si une année simple est transmise
        $labels = [];
        //dd(Carbon::getLocale());
        $sinceAnnee = null;
        $tillAnnee = null;

        if($annee)
        {
            if($since=="true")
            {

                if($vehicule)
                {
                    $maximum=DB::table('flux_finances')->selectRaw("MAX(YEAR(date_transaction)) as annee")->where('vehicule_id',$vehicule)->get()->first();
                }
                else
                {
                    $maximum=DB::table('flux_finances')->selectRaw("MAX(YEAR(date_transaction)) as annee")->get()->first();
                }
                
                if($maximum->annee)
                {
                    $base = $maximum->annee;
                    if($maximum->annee==Carbon::now()->year)
                    {
                        $base = "2018";
                    }
                    $sinceAnnee = Carbon::parse($base."-01-01")->locale('fr_FR');
                    $tillAnnee = Carbon::parse($annee."-12-31")->locale('fr_FR');
                    $period = CarbonPeriod::create($sinceAnnee, '1 month',$tillAnnee);
                   
                }
                else
                {
                    $annee = Carbon::now()->year();
                    $sinceAnnee = Carbon::parse($annee."-01-01")->locale('fr_FR');
                    $tillAnnee = Carbon::now()->locale('fr_FR');
                    $period = CarbonPeriod::create(Carbon::parse($annee."-01-01")->locale('fr_FR'), '1 month',Carbon::now()->locale('fr_FR'));
                }
                
            }
            else
            {
              Log::info('Pas de since'.$since);
              if($annee==Carbon::now()->year)
              {
                  $moisDeFin = Carbon::now()->month;
              }
              else
              {
                $moisDeFin = "12";
              }
              $period = CarbonPeriod::create(Carbon::parse($annee."-01-01")->locale('fr_FR'), '1 month',Carbon::parse($annee."-{$moisDeFin}-31")->locale('fr_FR'));

             }

            foreach ($period as $dt) 
            {
                array_push($labels,['month'=>$dt->translatedFormat("m-Y"),"montant"=>0]);

            }
            
            $data=DB::table('flux_finances')->selectRaw("concat(LPAD(CAST(MONTH(date_transaction) AS CHAR(2)), 2, '0'),'-',CAST(year(date_transaction) AS CHAR(4)))as month,sum(CASE WHEN  INSTR (trim(financiable_type),'Location')THEN montant ELSE -montant END) as montant")
            ->where(function ($query) use ($vehicule)
            {
                if($vehicule)
                $query->where('vehicule_id',$vehicule);
                
            })->orderBy('month','asc')
            ->groupBy('month')->get();
            
            $bars=DB::table("vehicules")->selectRaw("distinct vehicules.libelle, sum( case when flux_finances.montant is not null and INSTR(trim(flux_finances.financiable_type),'Location') then flux_finances.montant else 0 end) as entree, sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end) as sortie, (abs(sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Location') then flux_finances.montant else 0 end) - sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end))) as result")
            ->leftJoin("flux_finances","flux_finances.vehicule_id","=","vehicules.id")
            ->where(function ($query) use ($vehicule)
            {
                if($vehicule)
                $query->where('vehicule_id',$vehicule);
                
            })
            ->where(function ($query) use ($since,$sinceAnnee,$tillAnnee,$annee)
            {
                if($since=="true")
                $query->whereBetween("flux_finances.date_transaction",[$sinceAnnee->toDateString(),$tillAnnee->toDateString()]);
                else
                $query->whereYear("flux_finances.date_transaction","=",$annee);

            })->orderBy('libelle','asc')->groupBy("libelle")->get();
        
             $months =collect(['janv.', 'févr.', 'mars', 'avr.', 'mai', 'juin', 'juil.', 'août', 'sept.', 'oct.', 'nov.', 'déc.']);
             foreach($labels as $key=>$label)
             {
                 foreach($data as $d)
                 {
                     if($label['month']==$d->month)
                     {
                        $labels[$key]['montant'] =$d->montant;
                        break;
                     }
                 }
             }
            
             
        }

        if($d_dbt && $d_fin)
        {
           //
           $dbt = Carbon::parse($d_dbt);
           $fin = Carbon::parse($d_fin);
           //On vérifie si les 2 dates ont une différence d'au moins 30 jours
           $difference= $fin->diffInDays($dbt);

           if($difference>31)
           {
            $period = CarbonPeriod::create($dbt, '1 month',$fin);
            $byMonth = true;
           }
           else
           {
            $period = CarbonPeriod::create($dbt, '1 day',$fin);
            $byMonth = false;
           }

           //On liste chaque mois ou chaque jour de la periode temporelle
           foreach ($period as $dt) 
           {
               array_push($labels,['month'=>$dt->translatedFormat( $byMonth ? "m-Y" : "d-m-Y"),"montant"=>0]);
           }

           //Donnée du Line Chart
           $data=DB::table('flux_finances')->selectRaw( 
               $byMonth ? 
           "concat(LPAD(CAST(MONTH(date_transaction) AS CHAR(2)), 2, '0'),'-',CAST(year(date_transaction) AS CHAR(4)))as month,sum(CASE WHEN  INSTR (trim(financiable_type),'Location')THEN montant ELSE -montant END) as montant"
           :
           "concat(LPAD(CAST(DAY(date_transaction) AS CHAR(2)), 2, '0'),'-',LPAD(CAST(MONTH(date_transaction) AS CHAR(2)), 2, '0'),'-',CAST(year(date_transaction) AS CHAR(4)))as month,sum(CASE WHEN  INSTR (trim(financiable_type),'Location')THEN montant ELSE -montant END) as montant"
           )->where(function ($query) use ($vehicule)
           {
            if($vehicule)
               $query->where('vehicule_id',$vehicule);
            
           })->where(function ($query) use ($dbt,$fin)
           {
               
               $query->whereBetween("flux_finances.date_transaction",[$dbt->toDateString(),$fin->toDateString()]);

           })->orderBy('month','asc')->groupBy('month')->get();
           
           //Donnée chart bar
           $bars=DB::table("vehicules")->selectRaw("distinct vehicules.libelle, sum( case when flux_finances.montant is not null and INSTR(trim(flux_finances.financiable_type),'Location') then flux_finances.montant else 0 end) as entree, sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end) as sortie, (abs(sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Location') then flux_finances.montant else 0 end) - sum( case when flux_finances.montant is not null and INSTR (trim(financiable_type),'Depense') then flux_finances.montant else 0 end))) as result")
           ->leftJoin("flux_finances","flux_finances.vehicule_id","=","vehicules.id")
           ->where(function ($query) use ($vehicule)
           {
               if($vehicule)
               $query->where('vehicule_id',$vehicule);
               
           })
           ->where(function ($query) use ($dbt,$fin)
           {
               
               $query->whereBetween("flux_finances.date_transaction",[$dbt->toDateString(),$fin->toDateString()]);

           })->orderBy('libelle','asc')->groupBy("libelle")->get();

           
            
           //On ajuste les deux tableaux
           foreach($labels as $key=>$label)
           {
               
               foreach($data as $d)
               {
                   if($label['month']==$d->month)
                   {
                      $labels[$key]['montant'] =$d->montant;
                      break;
                   }
               }
           }

        }

        $data = $data->isNotEmpty() ?  $labels : []; 

        return [ "labels"=>isset($labels) ? $labels : $labels ,"ca"=> isset($data) ? $data : [],"bars"=>isset($bars) ? $bars: []];
    }
}
