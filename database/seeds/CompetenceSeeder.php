<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;


class CompetenceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            $competences = Config::get('competences');
    
            foreach($competences as $competence)
            {
                $a = ["libelle"=>$competence['libelle']];
                factory('App\Competence')->create($a);
            }
    }
}
