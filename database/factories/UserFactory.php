<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Formation;
use App\SecteurActivite;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {

    $formation = Formation::inRandomOrder()->first();
    $pays = Formation::inRandomOrder()->first();
    $secteur = SecteurActivite::inRandomOrder()->first();
    $type = $faker->randomElement(['membre', 'admin']);

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'description' => $faker->text,
        'avatar' => $faker->word,
        'role_id' => factory(App\Role::class),
        'type'=>$type,
        'username' => $faker->userName,
        'email_verified_at' => $faker->dateTime(),
        'password' => bcrypt("password"),
        'api_token' => Str::random(80),
        // 'remember_token' => Str::random(10),
        'last_sign_out_at' => $faker->dateTime(),
        'last_sign_in_at' => $faker->dateTime(),
        'nom' =>$type =="admin"  ? null : $faker->lastName ,
        'prenom' =>$type =="admin"  ? null : $faker->firstName ,
        'promotion' => $type =="admin"  ? null : $faker->numberBetween($min = 1950, $max = 2018).""  ,
        'civilite' =>  $type =="admin"  ? null : $faker->randomElement(['m.', 'mme', 'mlle', 'pr','dr']),
        'statut_marital' =>  $type =="admin"  ? null : $faker->randomElement(["célibataire","marié(e)","veuf(ve)","concubinage"]),
        'date_naissance' => $type =="admin"  ? null : $faker->date($format = 'Y-m-d', $max = '2000-01-01'), // '1979-06-09'
        'phone1' => $type =="admin"  ? null : $faker->phoneNumber,
        'phone2' =>  $type =="admin"  ? null : $faker->phoneNumber,
        'facebook' =>  $type =="admin"  ? null : "https://www.facebook.com/".$faker->word,
        'twitter' =>  $type =="admin"  ? null :"https://www.twitter.com/".$faker->word,
        'linkedin' =>  $type =="admin"  ? null : "https://www.linkedin.com/".$faker->word,
        'activite_actuelle' =>  $type =="admin"  ? null : $faker->word,
        'derniere_profession' =>  $type =="admin"  ? null : $faker->jobTitle,
        'secteur_activite_id' =>  $type =="admin"  ? null : factory(App\SecteurActivite::class),
        'formation_id' => $type =="admin"  ? null : factory(App\Formation::class),
        'pays_residence_id' => $type =="admin"  ? null : ( $pays ? $pays->id : factory(App\Pays::class)),
        'created_by' => factory(App\User::class),
    ];
});
