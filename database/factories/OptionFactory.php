<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Option::class, function (Faker $faker) {
    return [
        'libelle' => $faker->word,
        'type_option_id' => $faker->randomNumber(),
    ];
});
