<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Option::class, function (Faker $faker) {

    $libelle = "";
    do
    {
        $libelle =  $faker->unique($reset = true)->word;

    }while(\App\TypeMotorisation::whereLibelle($libelle)->first()!=null);
    return [
        "libelle" => $libelle,
        'type_option_id'=>function(){ return factory('App\TypeOption')->create()->id;}


    ];
});

$factory->define(App\TypeMotorisation::class, function (Faker $faker) {

    return [
        "libelle" => $faker->word."-".Str::random(5)

    ];
});



