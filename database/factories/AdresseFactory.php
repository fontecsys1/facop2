<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Adresse::class, function (Faker $faker) {
    return [
        'rue' => $faker->word,
        'nr' => $faker->word,
        'bp' => $faker->word,
        'quartier' => $faker->word,
        'ville' => $faker->word,
        'pays_id' =>factory(App\Pays::class),
    ];
});
