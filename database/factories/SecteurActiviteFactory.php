<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\SecteurActivite::class, function (Faker $faker) {
    return [
        'libelle' => $faker->word."".Str::random(4),
    ];
});
